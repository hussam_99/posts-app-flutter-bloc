import 'package:dartz/dartz.dart';
import '../../../../core/error/exception.dart';
import '../../../../core/error/failure.dart';
import '../../../../core/network/network_info.dart';
import '../dataSources/post_local_data_source.dart';
import '../dataSources/post_remote_data_source.dart';
import '../models/post_model.dart';
import '../../domain/entities/post.dart';
import '../../domain/repos/post_repo.dart';

//new type
typedef Future<Unit> DeleteOrUpdateOrAddPost();

class PostRepoImplement implements PostRepo {
  final PostRemoteDataSource postRemoteDataSource;
  final PostLocalDataSource postLocalDataSource;
  final NetworkInfo networkInfo;
  PostRepoImplement({
    required this.postRemoteDataSource,
    required this.postLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<Post>>> getAllPost() async {
    //we should check the internet connection it there is a intrnet connection I should get the info from api else I should get them from local database
    if (await networkInfo.isConnected) {
      try {
        final remotePosts = await postRemoteDataSource.getAllPost();
        postLocalDataSource.cachPosts(remotePosts);
        return Right(remotePosts);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localPosts = await postLocalDataSource.getCachedPosts();
        return Right(localPosts);
      } on EmptyCacheException {
        return Left(EmptyCacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Unit>> addPost(Post newPost) async {
    final postModel = PostModel(
      // id: newPost.id,
      title: newPost.title,
      body: newPost.body,
    );
    return _getMessage(() async {
      return postRemoteDataSource.addPost(postModel);
    });
    /* if (await networkInfo.isConnected) {
      try {
        await postRemoteDataSource.addPost(postModel);
        return const Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }*/
  }

  @override
  Future<Either<Failure, Unit>> deletePost(int id) async {
    return _getMessage(() async {
      return postRemoteDataSource.deletePost(id);
    });

    /* if (await networkInfo.isConnected) {
      try {
        await postRemoteDataSource.deletePost(id);
        return const Right(unit);
      } on ServerFailure {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }*/
  }

  @override
  Future<Either<Failure, Unit>> updatePost(Post newPost) async {
    final postModel = PostModel(
      id: newPost.id,
      title: newPost.title,
      body: newPost.body,
    );
    return _getMessage(() async {
      return postRemoteDataSource.updatePost(postModel);
    });
    /*  if (await networkInfo.isConnected) {
      try {
        await postRemoteDataSource.updatePost(postModel);
        return const Right(unit);
      } on ServerFailure {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }*/
  }

  Future<Either<Failure, Unit>> _getMessage(
      DeleteOrUpdateOrAddPost deleteOrUpdateOrAddPost) async {
    if (await networkInfo.isConnected) {
      try {
        return const Right(unit);
      } on ServerFailure {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
