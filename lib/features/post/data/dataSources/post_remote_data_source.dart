import 'dart:convert';

import '../../../../core/error/exception.dart';
import '../models/post_model.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

// Why Abstract???
// cuase in case I was using a package and this package gets failed or out of date
//so I can make implement of this class so many times with another package
abstract class PostRemoteDataSource {
  //the functions declations that I may invoke from Api
  Future<List<PostModel>> getAllPost();
  Future<Unit> deletePost(int id);
  Future<Unit> updatePost(PostModel post);
  Future<Unit> addPost(PostModel post);
}

const BASE_URL = 'https://jsonplaceholder.typicode.com';

class PostRemoteDataSourceImplement implements PostRemoteDataSource {
  final http.Client client;

  PostRemoteDataSourceImplement({
    required this.client,
  });

  @override
  Future<Unit> deletePost(int id) async {
    final response = await client.delete(
      Uri.parse('$BASE_URL/posts/${id}'),
      headers: {
        'Content-Type': 'application/',
      },
    );
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<PostModel>> getAllPost() async {
    final response = await client.get(
      Uri.parse('$BASE_URL/posts/'),
      headers: {
        'Content-Type': 'application/',
      },
    );

    if (response.statusCode == 200) {
      final List decodedJson = json.decode(response.body) as List;
      final List<PostModel> postModels = decodedJson
          .map<PostModel>((jsonPostModel) => PostModel.fromJson(jsonPostModel))
          .toList();
      return postModels;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> updatePost(PostModel post) async {
    final postId = post.id.toString();
    final body = {
      'title': post.title,
      'body': post.body,
    };

    final response = await client.patch(
      Uri.parse('$BASE_URL/posts/$postId'),
      body: body,
      headers: {
        'Content-Type': 'application/',
      },
    );
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Unit> addPost(PostModel post) async {
    final body = {
      'title': post.title,
      'body': post.body,
    };
    var response = await client.post(
      Uri.parse(BASE_URL + '/posts/'),
      body: body,
    );
    if (response.statusCode == 200) {
      return Future.value(unit);
    } else {
      throw ServerException();
    }
  }
}
