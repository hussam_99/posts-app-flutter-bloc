import 'dart:convert';

import '../../../../core/error/exception.dart';
import '../models/post_model.dart';
import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class PostLocalDataSource {
  Future<List<PostModel>> getCachedPosts();
  Future<Unit> cachPosts(List<PostModel> postModels);
}

const CACHED_POSTS = 'CACHED_POSTS';

class PostLocalDataSourceImplement implements PostLocalDataSource {
  final SharedPreferences sharedPrefrences;
  PostLocalDataSourceImplement(this.sharedPrefrences);

  @override
  Future<Unit> cachPosts(List<PostModel> postModels) {
    List postModelsToJson = postModels
        .map<Map<String, dynamic>>((postModel) => postModel.toJson())
        .toList();

    sharedPrefrences.setString(
      CACHED_POSTS,
      json.encode(postModelsToJson),
    );
    return Future.value(unit);
  }

  @override
  Future<List<PostModel>> getCachedPosts() {
    final jsonString = sharedPrefrences.getString(CACHED_POSTS);
    if (jsonString != null) {
      List decodedJson = json.decode(jsonString);
      List<PostModel> jsonToPostModels = decodedJson
          .map<PostModel>(
            (jsonPostModel) => PostModel.fromJson(jsonPostModel),
          )
          .toList();
      return Future.value(jsonToPostModels);
    } else {
      throw EmptyCacheException();
    }
  }
}
