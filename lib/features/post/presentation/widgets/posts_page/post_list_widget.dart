import 'package:flutter/material.dart';
import '../../../domain/entities/post.dart';
import '../../pages/post_details_page.dart';

class PostListWidget extends StatelessWidget {
  final List<Post> posts;
  const PostListWidget({super.key, required this.posts});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) {
        return ListTile(
          leading: Text(posts.elementAt(index).id.toString()),
          title: Text(posts.elementAt(index).title),
          subtitle: Text(posts.elementAt(index).body),
          contentPadding: EdgeInsets.symmetric(horizontal: 10),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PostDetailsPage(
                post: posts.elementAt(index),
              ),
            ));
          },
        );
      },
      separatorBuilder: (context, index) {
        return Divider();
      },
      itemCount: posts.length,
    );
  }
}
