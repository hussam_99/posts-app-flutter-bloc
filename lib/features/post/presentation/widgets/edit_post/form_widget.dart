// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:post_app/features/post/domain/entities/post.dart';
import 'package:post_app/features/post/presentation/bloc/add_delete_update_posts_bloc/add_delete_update_posts_bloc.dart';
import 'package:post_app/features/post/presentation/widgets/edit_post/form_submit_btn.dart';
import 'package:post_app/features/post/presentation/widgets/edit_post/form_text_field_widget.dart';

class FormWidget extends StatefulWidget {
  final bool isUpdatePost;
  final Post? post;
  const FormWidget({
    super.key,
    required this.isUpdatePost,
    this.post,
  });

  @override
  State<FormWidget> createState() => _FormWidgetState();
}

class _FormWidgetState extends State<FormWidget> {
  final formKey = GlobalKey<FormState>();

  TextEditingController titleController = TextEditingController();
  TextEditingController bodyController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.isUpdatePost) {
      titleController.text = widget.post!.title;
      bodyController.text = widget.post!.body;
    }
  }

  validateFormThenUpdateOrAddPost() {
    final isValid = formKey.currentState!.validate();
    if (isValid) {
      final post = Post(
        id: widget.post?.id,
        title: titleController.text,
        body: bodyController.text,
      );
      if (widget.isUpdatePost) {
        BlocProvider.of<AddDeleteUpdatePostsBloc>(context).add(
          UpdatePostEvent(
            post: post,
          ),
        );
      } else {
        BlocProvider.of<AddDeleteUpdatePostsBloc>(context).add(
          AddPostEvent(
            post: post,
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
//title Text Field
          FormTextFieldWidget(
            textFieldController: titleController,
            hintText: "title",
          ),
          //body Text Field

          FormTextFieldWidget(
            textFieldController: bodyController,
            hintText: "body",
            maxLines: 6,
            minLines: 3,
          ),

          //button
          FormSubmitBtn(
            isUpdatePost: widget.isUpdatePost,
            onPressed: validateFormThenUpdateOrAddPost,
          ),
        ],
      ),
    );
  }
}
