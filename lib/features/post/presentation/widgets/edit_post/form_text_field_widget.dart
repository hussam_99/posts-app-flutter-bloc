import 'package:flutter/material.dart';

class FormTextFieldWidget extends StatelessWidget {
  const FormTextFieldWidget({
    super.key,
    required this.textFieldController,
    required this.hintText,
    this.minLines,
    this.maxLines,
  });
  final TextEditingController textFieldController;
  final String hintText;
  final int? minLines;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: TextFormField(
        maxLines: maxLines,
        minLines: minLines,
        controller: textFieldController,
        validator: (value) => value!.isEmpty ? "field can't be empty" : null,
        decoration: InputDecoration(hintText: hintText),
      ),
    );
  }
}
