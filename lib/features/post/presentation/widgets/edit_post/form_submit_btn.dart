import 'package:flutter/material.dart';

class FormSubmitBtn extends StatelessWidget {
  final void Function() onPressed;
  final bool isUpdatePost;

  const FormSubmitBtn({
    super.key,
    required this.isUpdatePost,
    required this.onPressed,
  });
  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      onPressed: onPressed,
      icon: Icon(
        isUpdatePost ? Icons.edit : Icons.add,
      ),
      label: Text(
        isUpdatePost ? "Edit" : "Add",
      ),
    );
  }
}
