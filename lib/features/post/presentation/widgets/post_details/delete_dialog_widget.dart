import 'package:flutter/material.dart';
import '../../bloc/add_delete_update_posts_bloc/add_delete_update_posts_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DeleteDialogWidget extends StatelessWidget {
  const DeleteDialogWidget({super.key, required this.postId});
  final int postId;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Are you sure?"),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("No")),
        TextButton(
            onPressed: () {
              BlocProvider.of<AddDeleteUpdatePostsBloc>(context).add(
                DeletePostEvent(
                  postId: postId,
                ),
              );
            },
            child: Text("Yes")),
      ],
    );
  }
}
