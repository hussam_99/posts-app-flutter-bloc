part of 'add_delete_update_posts_bloc.dart';

sealed class AddDeleteUpdatePostsState extends Equatable {
  const AddDeleteUpdatePostsState();

  @override
  List<Object> get props => [];
}

final class AddDeleteUpdatePostsInitial extends AddDeleteUpdatePostsState {}

final class LoadingAddDeleteUpdatePostState extends AddDeleteUpdatePostsState {}

final class ErrorAddDeleteUpdatePostState extends AddDeleteUpdatePostsState {
  final String message;

  ErrorAddDeleteUpdatePostState({required this.message});

  @override
  List<Object> get props => [message];
}

class MessageAddDeleteUpdatePostState extends AddDeleteUpdatePostsState {
  final String message;

  MessageAddDeleteUpdatePostState({required this.message});

  @override
  List<Object> get props => [message];
}
