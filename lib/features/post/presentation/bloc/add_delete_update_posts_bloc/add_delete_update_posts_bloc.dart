// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:post_app/core/error/failure.dart';
import 'package:post_app/core/strings/failures.dart';
import 'package:post_app/core/strings/messages.dart';
import 'package:post_app/features/post/domain/entities/post.dart';
import 'package:post_app/features/post/domain/useCases/add_post.dart';
import 'package:post_app/features/post/domain/useCases/delete_post.dart';
import 'package:post_app/features/post/domain/useCases/update_post.dart';

part 'add_delete_update_posts_event.dart';
part 'add_delete_update_posts_state.dart';

class AddDeleteUpdatePostsBloc
    extends Bloc<AddDeleteUpdatePostsEvent, AddDeleteUpdatePostsState> {
  final AddPostUseCase addPostUseCase;
  final UpdatePostUseCase updatePostUseCase;
  final DeletePostUseCase deletePostUseCase;

  AddDeleteUpdatePostsBloc({
    required this.addPostUseCase,
    required this.updatePostUseCase,
    required this.deletePostUseCase,
  }) : super(AddDeleteUpdatePostsInitial()) {
    on<AddDeleteUpdatePostsEvent>((event, emit) async {
      if (event is AddPostEvent) {
        emit(LoadingAddDeleteUpdatePostState());

        final failureOrDoneMessage = await addPostUseCase(event.post);
        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, ADD_SUCCESS_MESSAGE));
        /*
        failureOrDoneMessage.fold(
          (failure) {
            emit(
              ErrorAddDeleteUpdatePostState(
                message: _mapFailureToMessage(
                  failure,
                ),
              ),
            );
          },
          (_) {
            emit(
              MessageAddDeleteUpdatePostState(message: ADD_SUCCESS_MESSAGE),
            );
          },
        );
      */
      } else if (event is DeletePostEvent) {
        emit(LoadingAddDeleteUpdatePostState());
        final failureOrDoneMessage = await deletePostUseCase(event.postId);

        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, DELETE_SUCCESS_MESSAGE));
/*
        failureOrDoneMessage.fold(
          (failure) {
            emit(
              ErrorAddDeleteUpdatePostState(
                message: _mapFailureToMessage(
                  failure,
                ),
              ),
            );
          },
          (_) {
            emit(
              MessageAddDeleteUpdatePostState(message: DELETE_SUCCESS_MESSAGE),
            );
          },
        );
     */
      } else if (event is UpdatePostEvent) {
        emit(LoadingAddDeleteUpdatePostState());
        final failureOrDoneMessage = await updatePostUseCase(event.post);
        emit(_eitherDoneMessageOrErrorState(
            failureOrDoneMessage, UPDATE_SUCCESS_MESSAGE));
        /* 
        failureOrDoneMessage.fold(
          (failure) {
            emit(
              ErrorAddDeleteUpdatePostState(
                message: _mapFailureToMessage(
                  failure,
                ),
              ),
            );
          },
          (_) {
            emit(
              MessageAddDeleteUpdatePostState(
                message: UPDATE_SUCCESS_MESSAGE,
              ),
            );
          },
        );
     */
      }
    });
  }
}
//############

AddDeleteUpdatePostsState _eitherDoneMessageOrErrorState(
  Either<Failure, Unit> either,
  String message,
) {
  return either.fold(
    (failure) {
      return ErrorAddDeleteUpdatePostState(
          message: _mapFailureToMessage(failure));
    },
    (_) {
      return MessageAddDeleteUpdatePostState(
        message: message,
      );
    },
  );
}

//############
String _mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return SERVER_FAILURE_MESSAGE;
    case OfflineFailure:
      return OFFLINE_FAILURE_MESSAGE;
    default:
      return "Unexpected Error , Please try again later .";
  }
}
