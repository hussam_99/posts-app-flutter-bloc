// ignore_for_file: type_literal_in_constant_pattern, depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:post_app/core/error/failure.dart';
import 'package:post_app/core/strings/failures.dart';
import 'package:post_app/features/post/domain/entities/post.dart';
import 'package:post_app/features/post/domain/useCases/get_all_posts.dart';
import 'package:dartz/dartz.dart';

part 'posts_event.dart';
part 'posts_state.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final GetAllPostsUseCase getAllPosts;

  PostsBloc(this.getAllPosts) : super(PostsInitial()) {
    on<PostsEvent>(
      (event, emit) async {
        if (event is GetAllPostsEvent) {
          emit(LoadingPostsState());
          final failureOrPosts = await getAllPosts.call();
          emit(_mapFailureOrPostsToState(failureOrPosts));
        } else if (event is RefreshPostsEvent) {
          emit(LoadingPostsState());
          final failureOrPosts = await getAllPosts.call();
          emit(_mapFailureOrPostsToState(failureOrPosts));
        }
      },
    );
  }

  PostsState _mapFailureOrPostsToState(
    Either<Failure, List<Post>> either,
  ) {
    return either.fold(
      (failure) => ErrorPostsState(message: _mapFailureToMessage(failure)),
      (posts) => LoadedPostsState(
        posts: posts,
      ),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case EmptyCacheFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
