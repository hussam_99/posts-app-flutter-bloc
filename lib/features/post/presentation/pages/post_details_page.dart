import 'package:flutter/material.dart';
import '../../../../core/util/snack_bars.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../domain/entities/post.dart';
import '../bloc/add_delete_update_posts_bloc/add_delete_update_posts_bloc.dart';
import 'post_add_update_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'posts_page.dart';
import '../widgets/post_details/delete_dialog_widget.dart';

class PostDetailsPage extends StatelessWidget {
  const PostDetailsPage({super.key, required this.post});
  final Post post;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Post Details",
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        child: Column(
          children: [
            Text(post.title),
            Text(post.body),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => PostAddUpdatePage(
                        isUpdatePost: true,
                        post: post,
                      ),
                    ));
                  },
                  icon: const Icon(Icons.edit),
                  label: const Text('edit'),
                ),
                ElevatedButton.icon(
                  onPressed: () => deleteDialog(context),
                  icon: const Icon(Icons.delete),
                  label: const Text('delete'),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Colors.redAccent,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  deleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return BlocConsumer<AddDeleteUpdatePostsBloc,
            AddDeleteUpdatePostsState>(
          listener: (context, state) {
            if (state is MessageAddDeleteUpdatePostState) {
              SnackBarMessage().showSuccessSnackBar(
                  message: state.message, context: context);

              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (context) => const PostsPage(),
                  ),
                  (route) => false);
            } else if (state is ErrorAddDeleteUpdatePostState) {
              Navigator.of(context).pop();
              SnackBarMessage().showSuccessSnackBar(
                  message: state.message, context: context);
            }
          },
          builder: (context, state) {
            if (state is LoadingAddDeleteUpdatePostState) {
              return const AlertDialog(
                title: LaodingWidget(),
              );
            }
            return DeleteDialogWidget(postId: post.id!);
          },
        );
      },
    );
  }
}
