import 'package:flutter/material.dart';
import '../../../../core/util/snack_bars.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../domain/entities/post.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/add_delete_update_posts_bloc/add_delete_update_posts_bloc.dart';
import 'posts_page.dart';
import '../widgets/edit_post/form_widget.dart';

class PostAddUpdatePage extends StatelessWidget {
  final Post? post;
  final bool isUpdatePost;
  const PostAddUpdatePage({
    super.key,
    this.post,
    required this.isUpdatePost,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          isUpdatePost ? "Edit Post" : "Add Post",
          style: const TextStyle(
            fontSize: 20,
            color: Colors.white,
          ),
        ),
      ),
      body: _buildBody(context),
    );
  }

  _buildBody(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child:
            BlocConsumer<AddDeleteUpdatePostsBloc, AddDeleteUpdatePostsState>(
          builder: (context, state) {
            if (state is LoadingAddDeleteUpdatePostState) {
              return const LaodingWidget();
            }
            return FormWidget(
                isUpdatePost: isUpdatePost, post: isUpdatePost ? post : null);
          },
          listener: (context, state) {
            if (state is MessageAddDeleteUpdatePostState) {
              SnackBarMessage().showSuccessSnackBar(
                  message: state.message, context: context);
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (context) => const PostsPage(),
                  ),
                  (route) => false);
            } else if (state is ErrorAddDeleteUpdatePostState) {
              SnackBarMessage().showErrorSnackBar(
                message: state.message,
                context: context,
              );
            }
          },
        ),
      ),
    );
  }
}
