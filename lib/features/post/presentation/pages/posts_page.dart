// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:post_app/core/widgets/loading_widget.dart';
import 'package:post_app/features/post/presentation/bloc/posts/posts_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:post_app/features/post/presentation/pages/post_add_update_page.dart';
import 'package:post_app/features/post/presentation/widgets/posts_page/message_display_widget.dart';
import 'package:post_app/features/post/presentation/widgets/posts_page/post_list_widget.dart';

class PostsPage extends StatelessWidget {
  const PostsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildappbar(),
      body: buildBody(),
      floatingActionButton: _buildFoaltingBtn(context),
    );
  }

  buildappbar() {
    return AppBar(
      title: const Text(
        "Posts App",
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      ),
    );
  }

  buildBody() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: BlocBuilder<PostsBloc, PostsState>(
        builder: (context, state) {
          if (state is LoadingPostsState) {
            return const LaodingWidget();
          } else if (state is LoadedPostsState) {
            return RefreshIndicator(
              onRefresh: () => _refreshData(context),
              child: PostListWidget(posts: state.posts),
            );
          } else if (state is ErrorPostsState) {
            return MessageDisplayWidget(message: state.message);
          }
          return const LaodingWidget();
        },
      ),
    );
  }

  _buildFoaltingBtn(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => PostAddUpdatePage(
              isUpdatePost: false,
            ),
          ),
        );
      },
      child: Icon(
        Icons.add,
      ),
    );
  }

  Future<void> _refreshData(BuildContext context) async {
    BlocProvider.of<PostsBloc>(context).add(RefreshPostsEvent());
  }
}
