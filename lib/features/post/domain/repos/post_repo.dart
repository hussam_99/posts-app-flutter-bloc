import 'dart:async';
import 'package:dartz/dartz.dart';
import '../../../../core/error/failure.dart';
import '../entities/post.dart';

abstract class PostRepo {
//use cases
  Future<Either<Failure, List<Post>>> getAllPost();
  Future<Either<Failure, Unit>> deletePost(int id);
  Future<Either<Failure, Unit>> updatePost(Post newPost);
  Future<Either<Failure, Unit>> addPost(Post newPost);
}

//Failure

