import '../../../../core/error/failure.dart';
import '../repos/post_repo.dart';
import 'package:dartz/dartz.dart';

class DeletePostUseCase {
  final PostRepo postRepo;
  DeletePostUseCase(this.postRepo);
  Future<Either<Failure, Unit>> call(int postId) async {
    return postRepo.deletePost(postId);
  }
}
