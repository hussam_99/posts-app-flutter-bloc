import '../../../../core/error/failure.dart';
import '../entities/post.dart';
import '../repos/post_repo.dart';
import 'package:dartz/dartz.dart';

class GetAllPostsUseCase {
  final PostRepo postRepo;
  GetAllPostsUseCase(this.postRepo);
  Future<Either<Failure, List<Post>>> call() async {
    return postRepo.getAllPost();
  }
}
