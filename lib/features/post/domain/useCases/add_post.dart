import '../../../../core/error/failure.dart';
import '../entities/post.dart';
import '../repos/post_repo.dart';
import 'package:dartz/dartz.dart';

class AddPostUseCase {
  final PostRepo postRepo;
  AddPostUseCase(this.postRepo);
  Future<Either<Failure, Unit>> call(Post post) async {
    return postRepo.addPost(post);
  }
}
