import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/post.dart';
import '../repos/post_repo.dart';

class UpdatePostUseCase {
  final PostRepo postRepo;
  UpdatePostUseCase(this.postRepo);
  Future<Either<Failure, Unit>> call(Post post) async {
    return postRepo.updatePost(post);
  }
}
