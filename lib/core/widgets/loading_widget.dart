import 'package:flutter/material.dart';
import '../Theme/app_theme.dart';

class LaodingWidget extends StatelessWidget {
  const LaodingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 200,
      ),
      child: SizedBox(
        width: 200,
        height: 200,
        child: CircularProgressIndicator(
          color: secondaryColor,
        ),
      ),
    );
  }
}
