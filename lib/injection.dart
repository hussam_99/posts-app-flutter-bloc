import 'package:get_it/get_it.dart';
import 'core/network/network_info.dart';
import 'features/post/data/dataSources/post_local_data_source.dart';
import 'features/post/data/dataSources/post_remote_data_source.dart';
import 'features/post/data/repos/post_repo_implement.dart';
import 'features/post/domain/repos/post_repo.dart';
import 'features/post/domain/useCases/add_post.dart';
import 'features/post/domain/useCases/delete_post.dart';
import 'features/post/domain/useCases/get_all_posts.dart';
import 'features/post/domain/useCases/update_post.dart';
import 'features/post/presentation/bloc/add_delete_update_posts_bloc/add_delete_update_posts_bloc.dart';
import 'features/post/presentation/bloc/posts/posts_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

final sl = GetIt.instance;

Future<void> init() async {
//Feature Post

//Register Bloc

  sl.registerFactory(() => PostsBloc(sl()));
  sl.registerFactory(() => AddDeleteUpdatePostsBloc(
        addPostUseCase: sl(),
        deletePostUseCase: sl(),
        updatePostUseCase: sl(),
      ));

//UseCases //make only one object
  sl.registerLazySingleton(() => GetAllPostsUseCase(sl()));
  sl.registerLazySingleton(() => AddPostUseCase(sl()));
  sl.registerLazySingleton(() => UpdatePostUseCase(sl()));
  sl.registerLazySingleton(() => DeletePostUseCase(sl()));
//Repo //Abstract class
  sl.registerLazySingleton<PostRepo>(() => PostRepoImplement(
        networkInfo: sl(),
        postLocalDataSource: sl(),
        postRemoteDataSource: sl(),
      ));
//DataSources
  sl.registerLazySingleton<PostRemoteDataSource>(
      () => PostRemoteDataSourceImplement(client: sl()));
  sl.registerLazySingleton<PostLocalDataSource>(
      () => PostLocalDataSourceImplement(sl()));

//Core
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImplement(sl()),
  );

//External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
